const express = require('express');
const morgan = require('morgan');
const path = require('path');
require('dotenv').config()

//Initializations
const app = express();
require('./db/database.connection');


//Setting
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api/v0/users', require('./routes/user.routes'));
app.use('/api/v0/companies', require('./routes/company.routes'));
app.use('/api/v0/companies-guests', require('./routes/company_guest.routes'));
app.use('/api/v0/activities', require('./routes/activity.routes'));
app.use('/', (req, res)=>{
    res.json({
        title: "It's Working",
        message: "Welcome to API to FSY Ciudad del Carmen 2021"
    });
});

//Server
app.listen(app.get('port'), ()=>{
    console.log('fsy server is up!...');
});