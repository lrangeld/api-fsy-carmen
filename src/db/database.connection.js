const mongoose = require('mongoose');
const { mongodb } = require('../configs');

mongoose.connect(mongodb.URI, 
    { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err.message));