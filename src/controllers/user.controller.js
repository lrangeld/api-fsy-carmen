const User = require('../models/user.model');
const jwt = require('jsonwebtoken');
const { secret } = require('../configs');

const userCtrl = {};

userCtrl.getUsers = async (req, res) => {
    try {
        const users = await User.find();
        return res.status(200).json({
            status: 'Success',
            data: users,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error obtener usuarios",
                body: error.message
            }
        });
    }
};

userCtrl.getUsersByType = async (req, res) => {
    try {
        const users = await User.find({ rol: req.params.usertype }, {createdAt: 0, updatedAt: 0});
        return res.status(200).json({
            status: 'Success',
            data: users,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error crear usuario",
                body: error.message
            }
        });
    }
};


userCtrl.addUser = async (req, res) => {
    try {
        // Validar nombre de usuario unico
        const nickValid = await User.findOne({nick: req.body.nick}, {password: 0, admin: 0, urlImage: 0, createdAt: 0, updatedAt:0});
        if(nickValid) {
            return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: 'Error al registrar',
                body: 'El nombre de usuario ya fue registrado'
                }
            });
        }

        //Guardar participante en la base de datos
        if(req.body.rol == 'guest'){
            const user = new User(req.body);
            await user.save();

            return res.status(201).json({
                status: 'Added',
                data: 'Participante añadido correctamente',
                error: null
            });
        }

        //Guardar usuario en la base de datos
        const user = new User(req.body);
        user.password = await user.encryptPassword(user.password)
        await user.save();

        //Construir Token
        const token = jwt.sign({
            idUser: user._id,
            rol: user.rol,
            name: user.name
        }, secret);

        //Enviar respuesta al usuario con token en encabezado
        return res.status(201).header('auth-token', token).json({
            status: 'Added',
            data: 'Usuario añadido correctamente',
            token,
            error: null
        });

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error crear usuario",
                body: error.message
            }
        });
    }
};

userCtrl.updateUserById = async (req, res) => {
    try {
        await User.findByIdAndUpdate(req.params.id, req.body);
        return res.status(201).json({
            status: 'Updated',
            data: 'Usuario actualizado correctamente',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al actualizar usuario",
                body: error.message
            }
        });
    }
};

userCtrl.deleteUserById = async (req, res) => {
    try {
        await User.findByIdAndRemove(req.params.id);
        return res.status(201).json({
            status: 'Deleted',
            data: 'Usuario eliminado',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al eliminar usuario",
                body: error.message
            }
        });
    }
}

userCtrl.login = async (req, res) => {
    try {
        //Buscar usuario
        const user = await User.findOne({ nick: req.body.nick });
        if (!user) return res.status(401).json({
            status: "Failed",
            data: null,
            error: {
                title: 'Error al inicar sesion',
                body: 'Las credenciales son incorrectas'
                }
            });

        //Validar contraseña
        const validPassword = await user.decryptPassword(req.body.password);
        if (!validPassword) return res.status(401).json({
            status: "Failed",
            data: null,
            error: {
                title: 'Error al inicar sesion',
                body: 'Las credenciales son incorrectas'
                }
            });

        //Construir token
        const token = jwt.sign({
            idUser: user._id,
            rol: user.rol,
            name: user.name
        }, secret);
        //Enviar token a usuario
        return res.header('auth-token', token).json({
            status: 'Success',
            data: { id: user._id, name: user.name,  rol: user.rol },
            token,
            error: null
        });
    } catch (error) {
        return res.status(401).json({
            status: "Failed",
            data: null,
            error: {
                title: 'Error al inicar sesion',
                body: error.message
                }
            });
    }
}

module.exports = userCtrl;