const Company = require('../models/company.model');

const companyCtrl = {};

companyCtrl.getAllCompanies = async (req, res) => {
    try {
        const companies = await Company.find();
        return res.status(200).json({
            status: 'Success',
            data: companies,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error obtener compañias",
                body: error.message
            }
        });
    }
}

companyCtrl.getCompanyById = async (req, res) => {
    try {
        const company = await Company.find({ _id: req.params.companyid }, {createdAt: 0, updatedAt: 0});;
        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error otener compañia",
                body: error.message
            }
        });
    }
}

companyCtrl.addCompany = async (req, res) => {
    try {
        const company = new Company(req.body);
        await company.save();

        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
        
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error agregar compañia",
                body: error.message
            }
        });
    }
}

companyCtrl.updateCompanyById = async (req, res) => {
    try {
        await Company.findByIdAndUpdate(req.params.companyid, req.body);
        return res.status(201).json({
            status: 'Updated',
            data: 'Compañia actualizado correctamente',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al actualizar compañia",
                body: error.message
            }
        });
    }
}

companyCtrl.deleteCompanyById = async (req, res) => {
    try {
        await Company.findByIdAndRemove(req.params.companyid);
        return res.status(201).json({
            status: 'Deleted',
            data: 'Compañia eliminada',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al eliminar Compañia",
                body: error.message
            }
        });
    }
}

companyCtrl.getCompanyByParentId = async (req, res) => {
    try {
        const company = await Company.find({ idParent: req.params.parentid }, {createdAt: 0, updatedAt: 0});
        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error otener compañia",
                body: error.message
            }
        });
    }
}

module.exports = companyCtrl;