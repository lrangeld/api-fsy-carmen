const CompanyGuest = require('../models/company_guest.model');

const companyGuestCtrl = {};

companyGuestCtrl.getAllCompaniesGuests = async (req, res) => {
    try {
        const companies = await CompanyGuest.find();
        return res.status(200).json({
            status: 'Success',
            data: companies,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error obtener compañias",
                body: error.message
            }
        });
    }
}

companyGuestCtrl.getCompanyGuestByGuestId = async (req, res) => {
    try {
        const company = await CompanyGuest.find({ idGuest: req.params.guestid }, {createdAt: 0, updatedAt: 0});;
        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error otener compañia",
                body: error.message
            }
        });
    }
}

companyGuestCtrl.getCompanyGuestByCompanyId = async (req, res) => {
    try {
        const company = await CompanyGuest.find({ idCompany: req.params.companyid }, {createdAt: 0, updatedAt: 0});
        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error otener compañia",
                body: error.message
            }
        });
    }
}

companyGuestCtrl.addCompanyGuest = async (req, res) => {
    try {
        const company = new CompanyGuest(req.body);
        await company.save();

        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
        
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error asignar invitado a compañia",
                body: error.message
            }
        });
    }
}


companyGuestCtrl.deleteCompanGuestyByGuestId = async (req, res) => {
    try {
        await CompanyGuest.findOneAndRemove({ idGuest: req.params.guestid });
        return res.status(201).json({
            status: 'Deleted',
            data: 'Compañia eliminada',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al eliminar Compañia",
                body: error.message
            }
        });
    }
}

companyGuestCtrl.getGuestsByCompanyId = async (req, res) => {
    try {
        const company = await CompanyGuest.find({ idCompany: req.params.companyid }, {createdAt: 0,updatedAt: 0}).populate('idGuest', 'name').populate('idCompany', 'name');
        
        return res.status(200).json({
            status: 'Success',
            data: company,
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error otener compañia",
                body: error.message
            }
        });
    }
}

module.exports = companyGuestCtrl;