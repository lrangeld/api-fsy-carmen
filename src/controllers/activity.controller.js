const ActivityModel = require('../models/activity.model');

const activityCtrl = {}

activityCtrl.getAllActivities = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({},{ createdAt: 0, updatedAt: 0 });
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error al recuperar actividades",
                body: error.message
            }
        });
    }
}

activityCtrl.getActivitiesByDay = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({ date: new Date(req.params.day) }, { createdAt: 0, updatedAt: 0 });
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error al otener actividades del día",
                body: error.message
            }
        });
    }
}

activityCtrl.getActivitiesByGuest = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({ idGuest: req.params.guest }, { createdAt: 0, updatedAt: 0 });
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error al recuperar actividades del participante",
                body: error.message
            }
        });
    }
}

activityCtrl.getActivitiesByDayAndGuest = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({ date: new Date(req.params.day), idGuest: req.params.guest }, { createdAt: 0, updatedAt: 0 });
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error al recuperar actividades por día del participante",
                body: error.message
            }
        });
    }
}

activityCtrl.addActivity = async (req, res) => {
    try {
        const activity = new ActivityModel(req.body);
        await activity.save();

        return res.status(200).json({
            status: 'Success',
            data: activity,
            error: null
        });
        
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al agregar actividad",
                body: error.message
            }
        });
    }
}

activityCtrl.updateActivityByDayAndGuest = async (req, res) => {
    try {
        await ActivityModel.findOneAndUpdate({ date: new Date(req.params.day), idGuest: req.params.guest }, req.body);
        return res.status(201).json({
            status: 'Updated',
            data: 'Actividad actualizada correctamente',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al actualizar actividad",
                body: error.message
            }
        });
    }
}

activityCtrl.deleteActivityByDayAndGuest = async (req, res) => {
    try {
        await Company.findOnedAndRemove({ date: new Date(req.params.day), idGuest: req.params.guest });
        return res.status(201).json({
            status: 'Deleted',
            data: 'actividad eliminada',
            error: null
        });
    } catch (error) {
        return res.status(400).json({
            status: "Failed",
            data: null,
            error: {
                title: "Error al eliminar actividad",
                body: error.message
            }
        });
    }
}
//------------------------------------------------------------------
//VERIFY
activityCtrl.getActivitiesByDayAndParent = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({date: new Date(res.params.day), rol: req.params.params}, {createdAt: 0, updatedAt: 0});
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error at get activities by day",
                body: error.message
            }
        });
    }
}

//VERIFY
activityCtrl.getActivitiesByParent = async (req, res)=>{
    try {
        const activities = await ActivityModel.find({rol: req.params.parent}, {createdAt: 0, updatedAt: 0});
    
        return res.json({
            status: 'Success',
            data: activities,
            error: null
        });
        
    } catch (error) {
        return res.json({
            status: 'Error',
            data: null,
            error: {
                title: "Error at get activities by parent",
                body: error.message
            }
        });
    }
}

module.exports = activityCtrl;