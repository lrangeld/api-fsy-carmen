const { Router } = require('express');
const router = Router();

const companyCtrl = require('../controllers/company.controller');

//Ruta para recuperar todas las compañias
router.get('/', companyCtrl.getAllCompanies);

//Ruta para recuperar compañias por id
router.get('/:companyid', companyCtrl.getCompanyById);

//Ruta para agregar compañia
router.post('/', companyCtrl.addCompany);

//Ruta para actualizar compañias por id
router.put('/:companyid', companyCtrl.updateCompanyById);

//Ruta para recuperar compañias por id
router.delete('/:companyid', companyCtrl.deleteCompanyById);

//Ruta para recuperar compañias por id de padre
router.get('/parents/:parentid', companyCtrl.getCompanyByParentId);


module.exports = router;