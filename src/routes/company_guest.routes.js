const { Router } = require('express');
const router = Router();

const companyGuestCtrl = require('../controllers/company_guest.controller');

//Ruta para recuperar todas las compañias
router.get('/', companyGuestCtrl.getAllCompaniesGuests);

//Ruta para recuperar compañias por id de participantes
router.get('/:guestid', companyGuestCtrl.getCompanyGuestByGuestId);

//Ruta para recuperar compañias por id de compañia
router.get('/:companyid', companyGuestCtrl.getCompanyGuestByCompanyId);

//Ruta para agregar usuario a compañia
router.post('/', companyGuestCtrl.addCompanyGuest);

//Ruta para eliminar participante de compañia (por id participante)
router.get('/:guestid', companyGuestCtrl.deleteCompanGuestyByGuestId);

//Ruta para recuperar invitados por id compañia
router.get('/guests/:companyid', companyGuestCtrl.getGuestsByCompanyId);


module.exports = router;