const { Router } =  require('express');
const router = Router();

const userCtrl = require('../controllers/user.controller');

//Ruta para obtener todos los usuarios registrados
router.get('/', userCtrl.getUsers);

//Ruta para recuperar usuarios por tipo
router.get('/:usertype', userCtrl.getUsersByType);

//Ruta para agregar usuario
router.post('/', userCtrl.addUser);

//Ruta para actualizar usuario
router.put('/update/:id', userCtrl.updateUserById);

//Ruta para iniciar sesion
router.put('/login', userCtrl.login);


module.exports = router;