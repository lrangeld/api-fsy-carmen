const { Router } =  require('express');
const router = Router();

const activityCtrl = require('../controllers/activity.controller');

//Ruta para recuperar todas las actividades
router.get('/', activityCtrl.getAllActivities);

//Ruta para recuperar actividades por dia
router.get('/:day', activityCtrl.getActivitiesByDay);

//Ruta para recuperar actividades por participante
router.get('/guest/:guest', activityCtrl.getActivitiesByGuest);

//Ruta para recuperar actividades por dia e invitado
router.get('/guest/:day/:guest', activityCtrl.getActivitiesByDayAndGuest);

//Ruta para agregar nueva actividad de participante
router.post('/', activityCtrl.addActivity);

//Ruta para modificar actividad por dia y participante
router.put('/guest/:day/:guest', activityCtrl.updateActivityByDayAndGuest);

//Ruta para eliminar actividad de participante y dia
router.delete('/guest/:day/:guest', activityCtrl.deleteActivityByDayAndGuest);

/** -------------------------------------------------------------------------- **/
//Ruta para recuperar actividades por dia y padre
router.get('/activities-parent/:day/:parent', activityCtrl.getActivitiesByDayAndParent);

//Ruta para recuperar actividades por padre
router.get('/activities-parent/:parent', activityCtrl.getActivitiesByParent);

module.exports = router;