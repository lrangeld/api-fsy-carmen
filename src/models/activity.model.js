const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');


const Activity = new Schema({
    date: {
        type: Date,
        required: true
    },
    inputDate: {
        type: Date
    },
    outputDate: {
        type: Date
    },
    idGuest: {//ref to match Other Schema (use populate at the query to generate join)
        ref: 'Users',
        type: Schema.Types.ObjectId
    }
},{ //Options schema
    timestamps: true,
    versionKey: false
});

module.exports = model('Activities', Activity);