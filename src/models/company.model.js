const { Schema, model } = require('mongoose');


const Company = new Schema({
    name: {
        type: String,
        required: true
    },
    idParent: {//ref to match Other Schema (use populate at the query to generate join)
        ref: 'Users',
        type: Schema.Types.ObjectId
    }
},{ //Options schema
    timestamps: true,
    versionKey: false
});

module.exports = model('Companies', Company);