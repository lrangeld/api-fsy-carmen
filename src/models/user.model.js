const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');


const User = new Schema({
    nick: {
        type: String
    },
    password: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    rol: {
        type: String,
        required: true
    },
    urlImage: {
        type: String
    }
},{ //Options schema
    timestamps: true,
    versionKey: false
});

User.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt)
}

User.methods.decryptPassword = async function (password) {
    return await bcrypt.compare(password, this.password);
}

module.exports = model('Users', User);