const { Schema, model } = require('mongoose');


const CompanyGuest = new Schema({
    idCompany: {//ref to match Other Schema (use populate at the query to generate join)
        ref: 'Companies',
        type: Schema.Types.ObjectId
    },
    idGuest: {//ref to match Other Schema (use populate at the query to generate join)
        ref: 'Users',
        type: Schema.Types.ObjectId
    }
},{ //Options schema
    timestamps: true,
    versionKey: false
});

module.exports = model('CompaniesGuests', CompanyGuest);