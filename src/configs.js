module.exports = {
    mongodb: {
        URI: `mongodb+srv://${process.env.DB_CLIENT_USER}:${process.env.DB_CLIENT_PASSWORD}@fsy-carmen.ju5tv.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
        

    },
    secret: process.env.SECRET_TOKEN,
    root_api: '/api/v1/'
};